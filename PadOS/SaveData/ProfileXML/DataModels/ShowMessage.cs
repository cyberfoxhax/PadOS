﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PadOS.SaveData.ProfileXML {
    public class ShowMessage: IAction {
        public string Text { get; set; }
    }
}
